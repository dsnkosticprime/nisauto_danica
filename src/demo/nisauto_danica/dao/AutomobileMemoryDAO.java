package demo.nisauto_danica.dao;

import demo.nisauto_danica.model.Automobile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

/**
 * Created by ninoslav on 10/10/2017.
 */
public class AutomobileMemoryDAO {

    private ArrayList<Automobile> catalogue = new ArrayList<Automobile>();


    public AutomobileMemoryDAO() {

        Automobile auto1 = new Automobile(101, "Audi a6", 2006, 7800,5,"metalic gray","yes",250,"http://thenewswheel.com/wp-content/uploads/2014/01/2014-Audi-A6-3.0T-Dakota-Gray-Metallic.jpg");
        Automobile auto2 = new Automobile(102, "Peugeot 306", 2008, 3800,3,"white","yes",210,"https://i.ytimg.com/vi/gGlO1T4cjao/maxresdefault.jpg");
        Automobile auto3 = new Automobile(103, "Opel Zafira", 2004, 5600,5,"gray","yes",220,"https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Opel_Zafira_Tourer_2.0_CDTI_Innovation_%28C%29_%E2%80%93_Frontansicht%2C_23._Mai_2013%2C_Heiligenhaus.jpg/1200px-Opel_Zafira_Tourer_2.0_CDTI_Innovation_%28C%29_%E2%80%93_Frontansicht%2C_23._Mai_2013%2C_Heiligenhaus.jpg");
        Automobile auto4 = new Automobile(104, "Reno Scenic", 2012, 6500,5,"turqoise","no",230,"http://www.conceptcarz.com/images/Renault/renault-Scenic-Exterior-Image-01.jpg");
        catalogue.add(auto1);
        catalogue.add(auto2);
        catalogue.add(auto3);
        catalogue.add(auto4);
    }


    public ArrayList<Automobile> getCatalogue() {

        return catalogue;

    }

    public Automobile showCatalogItem(int figure) {
        Automobile car = new Automobile();
        for (int i = 0; i < catalogue.size(); i++) {
            if (catalogue.get(i).getFigure() == figure) {
                car = catalogue.get(i);


            }

        } return car;



    }
}
