package demo.nisauto_danica.controller;

import demo.nisauto_danica.model.AddCarForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * Created by ninoslav on 10/25/2017.
 */
@Controller
public class AddCarController {

    @RequestMapping(value = "/dodajCar",method = RequestMethod.GET)
    public String showForm(Model model){
        AddCarForm addCarForm = new AddCarForm();
        model.addAttribute("carForm", addCarForm);
        return "add_car";
    }

    @RequestMapping(value="/submitCar", method = RequestMethod.POST)
    public String submitForm(@Valid @ModelAttribute("carForm") AddCarForm addCarForm, BindingResult bindingResult, Model model){
        if(bindingResult.hasErrors()){
            return "add_car";
        }
        model.addAttribute("figure", addCarForm.getFigure());
        model.addAttribute("name", addCarForm.getName());
        model.addAttribute("year", addCarForm.getYear());
        model.addAttribute("doorsCount", addCarForm.getDoorsConut());
        model.addAttribute("colour", addCarForm.getColour());
        model.addAttribute("registration", addCarForm.getRegistration());
        model.addAttribute("maxSpeed", addCarForm.getMaxSpeed());
        model.addAttribute("picture", addCarForm.getPicture());
        model.addAttribute("price", addCarForm.getPrice());

        return "test_submit";


    }
}
