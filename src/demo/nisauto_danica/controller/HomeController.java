package demo.nisauto_danica.controller;

import demo.nisauto_danica.dao.AutomobileMemoryDAO;
import demo.nisauto_danica.dto.BasicAutoDTO;
import demo.nisauto_danica.model.Automobile;
import demo.nisauto_danica.service.AutomobileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ninoslav on 10/10/2017.
 */
@Controller
public class HomeController {


    @Autowired
    private AutomobileService automobileService2;


    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String getData(Model model){

        ArrayList<BasicAutoDTO> katalog = automobileService2.getAllCars();

        model.addAttribute("katalog",katalog);

        return "home";

    }


}
