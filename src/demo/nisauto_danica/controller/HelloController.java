package demo.nisauto_danica.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by ninoslav on 10/9/2017.
 */
@Controller
@RequestMapping("/helloWorld")
public class HelloController {
    @RequestMapping (method = RequestMethod.GET)
    public String printHelloWorld(ModelMap model){
       model.addAttribute("message","Hello World :)");
        return "helloWorld";
    }




}
