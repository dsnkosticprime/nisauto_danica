package demo.nisauto_danica.controller;

import demo.nisauto_danica.dao.AutomobileMemoryDAO;
import demo.nisauto_danica.model.Automobile;
import demo.nisauto_danica.service.AutomobileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;

/**
 * Created by ninoslav on 10/16/2017.
 */
@Controller
public class CarAdController {

    @Autowired
    private AutomobileService automobileService;

    @RequestMapping(value = "/prikaziCar",method = RequestMethod.GET)
    public String getCarByFigure(@RequestParam("sifra") int figure, Model model){
        model.addAttribute("car",automobileService.getCar(figure));
        return "car_ad";



    }








}
