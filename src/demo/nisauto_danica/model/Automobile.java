package demo.nisauto_danica.model;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by ninoslav on 10/10/2017.
 */

public class Automobile {

   private int figure;
   private String name;
   private int year;
   private double price;
   private int doorsConut;
   private String colour;
   private String registration;
   private double maxSpeed;
   private String picture;

   public Automobile(){}

    public Automobile(int figure, String name, int year, double price,int doorsConut,String colour,String registration, double maxSpeed,String picture) {
        this.figure = figure;
        this.name = name;
        this.year = year;
        this.price = price;
        this.doorsConut=doorsConut;
        this.colour = colour;
        this.registration = registration;
        this.maxSpeed = maxSpeed;
        this.picture = picture;

    }

    public int getFigure() {
        return figure;
    }

    public void setFigure(int figure) {
        this.figure = figure;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getDoorsConut() {
        return doorsConut;
    }

    public void setDoorsConut(int doorsConut) {
        this.doorsConut = doorsConut;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public String toString() {
        return "Automobile{" +
                "figure=" + figure +
                ", name='" + name + '\'' +
                ", year=" + year +
                ", price=" + price +
                '}';
    }
}
