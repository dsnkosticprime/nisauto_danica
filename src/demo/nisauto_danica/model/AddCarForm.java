package demo.nisauto_danica.model;


import org.hibernate.validator.constraints.*;

import javax.validation.constraints.*;
import javax.validation.constraints.NotEmpty;


/**
 * Created by ninoslav on 10/27/2017.
 */
public class AddCarForm {


    @NotNull
    @Min(value = 1000)
    @Max(value = 9999)
    private Integer figure;

    @NotEmpty
    @Size(min=3,max = 60)
    private String name;
    private Integer year;
    @NotNull
    private Double price;
    @Min(value = 3)
    @Max(value = 5)
    private Integer doorsConut;
    private String colour;
    private Boolean registration;
    private Double maxSpeed;
    @NotNull
    @URL
    private String picture;


    public AddCarForm(){}

    public AddCarForm(int figure, String name, int year, double price, int doorsConut, String colour, String registration, double maxSpeed, String picture) {
        this.figure = figure;
        this.name = name;
        this.year = year;
        this.price = price;
        this.doorsConut=doorsConut;
        this.colour = colour;
        this.registration = false;
        this.maxSpeed = maxSpeed;
        this.picture = picture;

    }

    public Integer getFigure() {
        return figure;
    }

    public void setFigure(Integer figure) {
        this.figure = figure;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getDoorsConut() {
        return doorsConut;
    }

    public void setDoorsConut(Integer doorsConut) {
        this.doorsConut = doorsConut;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public Boolean getRegistration() {
        return registration;
    }

    public void setRegistration(Boolean registration) {
        this.registration = registration;
    }

    public Double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(Double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }


}
