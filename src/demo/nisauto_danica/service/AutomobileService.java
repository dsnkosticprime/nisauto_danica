package demo.nisauto_danica.service;

import demo.nisauto_danica.dao.AutomobileMemoryDAO;
import demo.nisauto_danica.dto.BasicAutoDTO;
import demo.nisauto_danica.dto.FullAutoDTO;
import demo.nisauto_danica.model.Automobile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by ninoslav on 10/19/2017.
 */
public class AutomobileService {



    private AutomobileMemoryDAO automobileMemoryDAO;

//    private ArrayList<Automobile>transferLista;
//    private ArrayList<BasicAutoDTO> katalog = new ArrayList<BasicAutoDTO>();





    public ArrayList<BasicAutoDTO> getAllCars (){
        ArrayList<Automobile>transferLista = automobileMemoryDAO.getCatalogue();
        ArrayList<BasicAutoDTO> katalog = new ArrayList<BasicAutoDTO>();

        for(int i=0; i< transferLista.size(); i++){
           Automobile car = transferLista.get(i);
           katalog.add(new BasicAutoDTO(car));
        }
        return katalog;}



    public FullAutoDTO getCar(int figure) {


     Automobile auto = automobileMemoryDAO.showCatalogItem(figure);
     return new FullAutoDTO(auto);

    }

    @Required
    public void setAutomobileMemoryDAO(AutomobileMemoryDAO automobileMemoryDAO) {
        this.automobileMemoryDAO = automobileMemoryDAO;
    }

    public AutomobileMemoryDAO getAutomobileMemoryDAO() {
        return automobileMemoryDAO;
    }
}








