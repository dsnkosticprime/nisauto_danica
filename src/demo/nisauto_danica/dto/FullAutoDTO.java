package demo.nisauto_danica.dto;

import demo.nisauto_danica.model.Automobile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by ninoslav on 10/19/2017.
 */
@Component
public class FullAutoDTO {



    private int figure;
    private String name;
    private int year;
    private double price;
    private int doorsConut;
    private String colour;
    private String registration;
    private double maxSpeed;
    private String picture;

    public FullAutoDTO(){

    }

    public FullAutoDTO(Automobile car) {
        this.figure = car.getFigure();
        this.name = car.getName();
        this.year = car.getYear();
        this.price = car.getPrice();
        this.doorsConut = car.getDoorsConut();
        this.colour = car.getColour();
        this.registration = car.getRegistration();
        this.maxSpeed = car.getMaxSpeed();
        this.picture = car.getPicture();
    }

    public int getFigure() {
        return figure;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }

    public double getPrice() {
        return price;
    }

    public int getDoorsConut() {
        return doorsConut;
    }

    public String getColour() {
        return colour;
    }

    public String getRegistration() {
        return registration;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public String getPicture() {
        return picture;
    }
}


