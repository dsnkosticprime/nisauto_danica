package demo.nisauto_danica.dto;

import demo.nisauto_danica.model.Automobile;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by ninoslav on 10/19/2017.
 */
public class BasicAutoDTO {


private Automobile car;
    private int figure;
    private String name;
    private int year;
    private double price;

    public BasicAutoDTO(Automobile car) {
        this.figure = car.getFigure();
        this.name = car.getName();
        this.year = car.getYear();
        this.price = car.getPrice();

    }

    public Automobile getCar() {
        return car;
    }

    public int getFigure() {
        return figure;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }

    public double getPrice() {
        return price;
    }
}

