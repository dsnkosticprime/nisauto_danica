<%--
  Created by IntelliJ IDEA.
  User: ninoslav
  Date: 10/16/2017
  Time: 3:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Car Ad</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
   <style>
       h1{
           color: white;
           font-family: "Microsoft Sans Serif";
       }
       footer{
           font-size: 15px;
           position: static;
       }
       .logo{
           float: left;
       }
       .slika{


       }
       .datas{
       }
       td {
           border: 2px solid grey;
           text-align: center;
           padding: 20px;

       }
       td:hover{
           background-color: gainsboro;
       }
       th {
           background-color: grey;
           border: 2px solid grey;
           text-align: center;
           padding: 20px;
           border-style: groove;
           color: white;

       }
       table {
           font-family: "Microsoft Sans Serif";
           font-size: large;
           width: 1200px;
           height: 500px;

       }
       h2{
           font-weight: 700;
       }

       .content{
           max-width: 1200px;
           margin: auto;
       }
       .dugmence {
           background-color: grey;
           border: groove;
           color: white;
           padding: 20px 20px;
           text-align: center;
           text-decoration: solid;
           font-family: "Microsoft Sans Serif";
           font-size: 25px;
           width: 400px;
           float: right;
           border-radius: 15px;
           box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
           -webkit-transition-duration: 0.4s;
           transition-duration: 0.4s;

       }
       .dugmence:hover {
           background-color: gainsboro;
           color: grey;
       }


   </style>
</head>
<body>



<div class="w3-container w3-gray">
    <div class="logo"><img src="resources/logo.png"alt="logo" style="width:70px;height:70px;"></div>
    <h1>Niš Auto</h1>
</div>
<div class="content">
<h2>Automobil šifra: ${car.figure}</h2>

<div class="slika">
    <img src="${car.picture}" alt="slika" style="width:1200px;height:650px;">
    </div>
<br>
<br>
    <table class="datas">

        <tr>
            <th>Ime i model automobila</th>
            <td>${car.name}</td>
        </tr>
        <tr>
            <th>Godina proizvodnje</th>
            <td>${car.year}</td>
        </tr>
        <tr>
            <th>Broj vrata</th>
           <td>${car.doorsConut}</td>
        </tr>
        <tr>
           <th>Boja</th>
            <td>${car.colour}</td>
        </tr>
        <tr>
            <th>Registracija</th>
            <td>${car.registration}</td>
        </tr>

        <tr>
           <th>Maksimalna brzina</th>
            <td>${car.maxSpeed} km/h </td>
        </tr>

    </table>
    <h2>Cena automobila: ${car.price} eur</h2>

    <%--<button class="dugmence" type="button" onclick="href='http://localhost:8080/'">Home</button>--%>
    <input class="dugmence" type=button onClick="parent.location='http://localhost:8080/'" value='HOME'>

</div>
<br>
<br>
<br>
<br>
<br>
<br>


<footer class="w3-container w3-gray">
    <p>Copyright Niš Auto © 2017. All Rights Reserved</p>
</footer>

</body>

</html>
