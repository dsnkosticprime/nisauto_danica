<%--
  Created by IntelliJ IDEA.
  User: ninoslav
  Date: 10/27/2017
  Time: 11:08 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
    <title>Submit</title>
</head>
<body>
<h1>Uspesno ste uneli oglas!</h1><br>
<p>Sifra automobile: ${figure}</p><br>
<p>Ime automobila: ${name}</p><br>
<p>Godina proizvodnje: ${year}</p><br>
<p>Broja vrata: ${doorsCount}</p><br>
<p>Da li je auto registrovan?  ${registration}</p><br>
<p>Maksimalna brzina automobila: ${maxSpeed}</p><br>
<p>Slika automobila:</p><br>
<img src="${picture}">
<br>
<h3>Cena automobila: ${price}</h3>



</body>
</html>
