<%--
  Created by IntelliJ IDEA.
  User: ninoslav
  Date: 10/10/2017
  Time: 2:32 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>



<html>
<head>
    <title>Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <style>

        h1{
            color: white;
            font-family: "Microsoft Sans Serif";
        }

        .slika{

            size: 1200px;
        }
        table {
            font-family: "Microsoft Sans Serif";
            font-size: large;
            border-collapse: collapse;
            width: 1920px;
            height: 500px;

        }
        td {
            border: 2px solid grey;
            text-align: center;
            padding: 20px;

        }
        th {
            background-color: grey;
            border: 2px solid grey;
            text-align: center;
            padding: 20px;
            border-style: groove;
            color: white;

        }
        footer{
            font-size: 15px;
        }
        .dugmence {
            background-color: grey;
            border: groove;
            color: white;
            padding: 20px 20px;
            text-align: center;
            text-decoration: solid;
            font-family: "Microsoft Sans Serif";
            font-size: 25px;
            width: 400px;
            float: right;
            border-radius: 15px;
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
            -webkit-transition-duration: 0.4s;
            transition-duration: 0.4s;

        }
        .dugmence:hover {
            background-color: gainsboro;
            color: grey;
        }
        .logo{
            float: left;
        }
        .name{
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
            -webkit-transition-duration: 0.4s;
            transition-duration: 0.4s;

        }
        .name:hover {
            background-color: gainsboro;
            color: grey;
        }
        .slicka:hover{
            background-color: gainsboro;
            color: grey;

        }





    </style>
</head>
<body>
<div class="w3-container w3-gray">
    <div class="logo"><img src="resources/logo.png"alt="logo" style="width:70px;height:70px;"></div>
    <h1>Niš Auto</h1>
</div>




<div class="slika">
    <img src="resources/background3.jpg" alt="slika" style="width:1920px;height:680px;">
</div>





<c:if test="${not empty katalog}">


    <table>
        <tr>
            <th><img src="resources/sort1.png"alt="sort" style="width:25px;height:25px;"> ŠIFRA </th>
            <th><img src="resources/sort1.png"alt="sort" style="width:25px;height:25px;"> IME AUTOMOBILA</th>
            <th><img src="resources/sort1.png"alt="sort" style="width:25px;height:25px;"> GODINA PROIZVODNJE</th>
            <th><img src="resources/sort1.png"alt="sort" style="width:25px;height:25px;"> CENA</th>
            <th>DETALJI</th>
        </tr>

        <c:forEach  var="object" items="${katalog}" >

            <tr>
                    <td>${object.figure}</td>
            <td class="name"><a href="http://localhost:8080/prikaziCar?sifra=${object.figure}">
                    ${object.name}
            </a>
            </td>
            <td>${object.year}</td>
            <td>${object.price}</td>
            <td> <a href="http://localhost:8080/prikaziCar?sifra=${object.figure}">
                <img class="slicka" src="resources/details1.png"alt="more"style="width:50px;height:50px;">
            </a>
                <img src="resources/edit111.png"alt="edit" style="width:50px;height:50px;">
                <img src="resources/delete1.png"alt="delete" style="width:50px;height:50px;"></td>
                </tr>

        </c:forEach>

    </table>


</c:if>
<br>
<br>
<button class="dugmence" type="button" onclick="parent.location= 'http://localhost:8080/dodajCar'">Dodaj oglas</button>

<br>
<br>
<br>
<br>
<br>
<br>


<footer class="w3-container w3-gray">
    <p>Copyright Niš Auto © 2017. All Rights Reserved</p>
</footer>



</body>
</html>
