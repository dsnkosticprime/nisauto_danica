<%--
  Created by IntelliJ IDEA.
  User: ninoslav
  Date: 10/25/2017
  Time: 10:34 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
    <title>Dodaj oglas</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <style>
        
        body{
            background-image: url("resources/background.jpg");
            background-repeat: no-repeat;
            background-position: center;
            background-attachment: fixed;


        }

        h1{
            color: white;
            font-family: "Microsoft Sans Serif";
        }

        .logo{
            float: left;
        }
        .dugmence {
            background-color: grey;
            border: groove;
            color: white;
            text-align: center;
            text-decoration: solid;
            font-family: "Microsoft Sans Serif";
            font-size: 25px;
            float: right;
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
            -webkit-transition-duration: 0.4s;
            transition-duration: 0.4s;
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border-radius: 5px;
            box-sizing: border-box;

        }
        .dugmence1 {
            background-color: yellowgreen;
            border: groove;
            color: white;
            text-align: center;
            text-decoration: solid;
            font-family: "Microsoft Sans Serif";
            font-size: 25px;
            float: right;
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
            -webkit-transition-duration: 0.4s;
            transition-duration: 0.4s;
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border-radius: 5px;
            box-sizing: border-box;

        }
        .dugmence:hover {
            background-color: gainsboro;
            color: grey;
        }
        .dugmence1:hover {
            background-color: greenyellow;
            color: darkgreen;
        }
       input[type=text]{
           width: 100%;
           padding: 12px 20px;
           margin: 8px 0;
           display: inline-block;
           border: inset;
           border-radius: 5px;
           box-sizing: border-box;

       }
       form{
            width: 1200px;
            margin: auto;

        }
        .error{
            color: red;
            font-weight: 700;
            font-size: medium;
        }
        .squaredTwo {
            width: 28px;
            height: 28px;
            position: relative;
            margin: 20px auto;
            background: #fcfff4;
            background: linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
            box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0,0,0,0.5);}
        label {
            width: 20px;
            height: 20px;
            cursor: pointer;
            position: absolute;
            left: 4px;
            top: 4px;
            background: linear-gradient(top, #222 0%, #45484d 100%);
            box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,1);}








        h3{
            font-weight: 700;
            font-size: medium;

        }








    </style>
</head>
<body>
<div class="w3-container w3-gray">
    <div class="logo"><img src="resources/logo.png"alt="logo" style="width:70px;height:70px;"></div>
    <h1>Niš Auto</h1>
</div>


<form:form method="POST" modelAttribute="carForm" action="/submitCar">

<br>
    <form:input path="figure" type="text" placeholder="Jedinstvena šifra automobila: *"/>
    <form:errors path="figure" cssClass="error"/>
  <br>
    <form:input path="name" type="text" placeholder="Naziv automobila: *"/>
    <form:errors path="name" cssClass="error"/>
    <br>
    <form:input path="year" type="text" name="year" placeholder="Godina proizvodnje:"/>
    <form:errors path="year" cssClass="error"/>
     <br>
    <form:input path="doorsConut" type="text" placeholder="Broj vrata:"/>
    <form:errors path="doorsConut" cssClass="error"/>
     <br>
    <form:input path="colour" type="text" placeholder="Boja automobila:"/><br>
    <form:errors path="colour" cssClass="error"/>

     <br>
    <%--<form:input path="registration" type="text" placeholder="Da li je automobil registrovan?"/><br>--%>
     <br>
    <form:input path="maxSpeed" type="text" placeholder="Maksimalna brzina automobila:"/><br>
    <form:errors path="maxSpeed" cssClass="error"/>
     <br>
    <form:input path="picture" type="text" placeholder="Upload URL slike automobila:"/><br>
    <form:errors path="picture" cssClass="error"/>
     <br>
    <form:input path="price" type="text" placeholder="Cena automobila: *"/>
    <form:errors path="price" cssClass="error"/>
    <h3>Da li je automobil registrovan?</h3> <form:checkbox path="registration" cssStyle="align-content: flex-end" cssClass="squaredTwo"/>
    <br>
    <input type= "submit" class="dugmence" value='Dodaj'><br>
    <input class="dugmence1" type=button onClick="parent.location='http://localhost:8080/'" value='Odustani'>



</form:form>

<br>
<br>
<br>
<br>
<br>
<br>
<br>

<div class="w3-container w3-gray">
    <p>Copyright Niš Auto © 2017. All Rights Reserved</p>

</div>


</body>
</html>
